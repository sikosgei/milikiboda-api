const jwt = require('jsonwebtoken');
const pool = require('./db');

const authenticateToken = (req, res, next) => {
    const authHeader = req.headers['authorization'];
    const token = authHeader && authHeader.split(' ')[1];
    if (!token) return res.sendStatus(401);

    jwt.verify(token, 'pass123', (err, user) => {
        if (err) return res.sendStatus(403);
        req.user = user;
        next();
    });
};

const authenticateAdmin = async (req, res, next) => {
    const userId = req.user.id;
    const result = await pool.query('SELECT * FROM users WHERE id = $1', [userId]);
    const user = result.rows[0];
    if (user && user.isadmin) {
        next();
    } else {
        res.sendStatus(403);
    }
};

module.exports = {
    authenticateToken,
    authenticateAdmin,
};
