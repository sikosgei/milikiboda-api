const express = require('express');
const { check, body, validationResult } = require('express-validator');
const { register, login, listUsers, getUser, updateUserDetails, removeUser,validateToken } = require('./controllers');
const { authenticateToken, authenticateAdmin } = require('./middleware');
const router = express.Router();

router.post('/auth/register', [
    check('name').notEmpty().withMessage('Name is required'),
    check('email').isEmail().withMessage('Valid email is required'),
    check('password').isLength({ min: 6 }).withMessage('Password must be at least 6 characters long'),
    check('phone').notEmpty().isLength({ min: 10 },{max:12}).withMessage('Phone number is required'),
    check('isAdmin').isBoolean().withMessage('isAdmin must be a boolean'),
    body('password_confirmation').custom((value, { req }) => {
        if (value !== req.body.password) {
            throw new Error('Password confirmation does not match password');
        }
        return true;
    })
], register);

router.post('/auth/login', [
    check('email').isEmail().withMessage('Valid email is required'),
    check('password').notEmpty().withMessage('Password is required')
], login);

router.get('/users', authenticateToken, listUsers);
router.get('/users/:id', authenticateToken, getUser);
router.get('/api/auth/validate-token', validateToken);
router.post('/users', [
    authenticateToken,
    authenticateAdmin,
    check('name').notEmpty().withMessage('Name is required'),
    check('email').isEmail().withMessage('Valid email is required'),
    check('password').isLength({ min: 6 }).withMessage('Password must be at least 6 characters long'),
    check('phone').notEmpty().withMessage('Phone number is required'),
    check('isAdmin').isBoolean().withMessage('isAdmin must be a boolean')
], register);
router.put('/users/:id', authenticateToken, updateUserDetails);
router.delete('/users/:id', authenticateToken, authenticateAdmin, removeUser);

module.exports = router;
