const pool = require('./db');

const getUserByEmail = async (email) => {
    const result = await pool.query('SELECT * FROM users WHERE email = $1', [email]);
    return result.rows[0];
};

const createUser = async (name, email, password, phone, isAdmin) => {
    const result = await pool.query(
        'INSERT INTO users (name, email, password, phone, isAdmin) VALUES ($1, $2, $3, $4, $5) RETURNING *',
        [name, email, password, phone, isAdmin]
    );
    return result.rows[0];
};

const getUsers = async () => {
    const result = await pool.query('SELECT * FROM users');
    return result.rows;
};

const getUserById = async (id) => {
    const result = await pool.query('SELECT * FROM users WHERE id = $1', [id]);
    return result.rows[0];
};

const updateUser = async (id, updates) => {
    const { name, email, password, phone, isAdmin } = updates;
    const result = await pool.query(
        'UPDATE users SET name = $1, email = $2, password = $3, phone = $4, isAdmin = $5 WHERE id = $6 RETURNING *',
        [name, email, password, phone, isAdmin, id]
    );
    return result.rows[0];
};

const deleteUser = async (id) => {
    await pool.query('DELETE FROM users WHERE id = $1', [id]);
};

module.exports = {
    getUserByEmail,
    createUser,
    getUsers,
    getUserById,
    updateUser,
    deleteUser
};
