const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const { validationResult } = require('express-validator');
const { getUserByEmail, createUser, getUsers, getUserById, updateUser, deleteUser } = require('./models');

const register = async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
    }

    const { name, email, password, password_confirmation, phone, isAdmin } = req.body;

    if (password !== password_confirmation) {
        return res.status(400).json({ errors: [{ msg: 'Password confirmation does not match password' }] });
    }

    const existingUser = await getUserByEmail(email);
    if (existingUser) {
        return res.status(400).json({ errors: [{ msg: 'Email is already in use' }] });
    }

    const hashedPassword = await bcrypt.hash(password, 10);
    try {
        const user = await createUser(name, email, hashedPassword, phone, isAdmin);
        res.status(201).json(user);
    } catch (error) {
        res.status(400).json({ error: error.message });
    }
};

const login = async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
    }

    const { email, password } = req.body;
    const user = await getUserByEmail(email);
    if (user && (await bcrypt.compare(password, user.password))) {
        const token = jwt.sign({ id: user.id, isAdmin: user.isAdmin }, 'pass123', { expiresIn: '1h' });
        res.json({ token });
    } else {
        res.status(401).send('Invalid credentials');
    }
};

const validateToken = (req, res) => {
    const token = req.headers['authorization']?.split(' ')[1];

    if (!token) {
        return res.status(401).json({ message: 'No token provided' });
    }

    jwt.verify(token, 'pass123', (err, decoded) => {
        if (err) {
            return res.status(401).json({ message: 'Failed to authenticate token' });
        }
        res.status(200).json({ message: 'Token is valid', userId: decoded.id, isAdmin: decoded.isAdmin });
    });
};

const listUsers = async (req, res) => {
    try {
        const users = await getUsers();
        res.json(users);
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};

const getUser = async (req, res) => {
    try {
        const user = await getUserById(req.params.id);
        if (user) {
            res.json(user);
        } else {
            res.status(404).send('User not found');
        }
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};

const updateUserDetails = async (req, res) => {
    try {
        const user = await updateUser(req.params.id, req.body);
        res.json(user);
    } catch (error) {
        res.status(400).json({ error: error.message });
    }
};

const removeUser = async (req, res) => {
    try {
        const user = await getUserById(req.params.id);
        if (!user) {
            return res.status(404).send('User not found');
        }
        await deleteUser(req.params.id);
        return res.status(204).json({ message: 'User deleted successfully' });
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};

module.exports = {
    register,
    login,
    validateToken,
    listUsers,
    getUser,
    updateUserDetails,
    removeUser,
};
