-- setup.sql

-- Create the database
CREATE DATABASE milikiboda;

-- Connect to the database
\c milikiboda;

-- Create the user
CREATE USER root WITH ENCRYPTED PASSWORD 'silah@123';

-- Grant privileges to the user
GRANT ALL PRIVILEGES ON DATABASE milikiboda TO root;

-- Create the users table
CREATE TABLE users (
                       id SERIAL PRIMARY KEY,
                       name VARCHAR(100),
                       email VARCHAR(100) UNIQUE,
                       password VARCHAR(100),
                       phone VARCHAR(15),
                       isAdmin BOOLEAN DEFAULT FALSE
);
